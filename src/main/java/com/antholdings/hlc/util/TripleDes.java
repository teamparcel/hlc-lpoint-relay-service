package com.antholdings.hlc.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESedeKeySpec;

/**
 * Created by hmludens on 2016. 3. 9..
 */
public class TripleDes {
    SecretKey secretKey = null;
    sun.misc.BASE64Encoder encoder;
    byte[] keyBytes;

    public TripleDes() {
        SecretKey secretKey = null;
        encoder = new sun.misc.BASE64Encoder();
        keyBytes = new byte[]
                {
                        0x4C, 0x33, 0x38, 0x30, 0x4C, 0x33, 0x38, 0x30, 0x32, 0x30, 0x31, 0x36, 0x30, 0x32, 0x32, 0x32, 0x4C, 0x4C, 0x43, 0x5F, 0x5F, 0x5F, 0x5F, 0x5F
                };
        keySet();
    }

    public void keySet() {
        System.out.println("1. 암호화 키 설정");

        try {
            DESedeKeySpec desKeySpec = new DESedeKeySpec(keyBytes);
            SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("TripleDes");
            secretKey = keyFactory.generateSecret(desKeySpec);

            System.out.println("2. 키 설정 완료");
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
        }
    }

    public byte[] Encryption(String orgData) {
        byte[] encByte = null;
        try {
            Cipher cipher = Cipher.getInstance("DESede/ECB/NoPadding");
            cipher.init(Cipher.ENCRYPT_MODE, secretKey);

            byte[] orgBufByte = orgData.getBytes("UTF-8");
            byte[] encBlockBuf = new byte[1024];
            encByte = new byte[orgBufByte.length];

            int nBlock = orgBufByte.length / 8;
            int reminder = orgBufByte.length % 8;

            /* 데이터가 없을 때 */
            if (nBlock == 0 && reminder == 0)
                return encByte;

            /* 길이가 8의 배수가 아닐 때 */
            if (reminder != 0) {
                /* 길이가 8 이상 일 때 */
                if (nBlock != 0) {
                    encBlockBuf = cipher.doFinal(orgBufByte, 0, nBlock * 8);

                    for (int i = 0; i < nBlock * 8; i++)
                        encByte[i] = encBlockBuf[i];

                    for (int i = 0; i < reminder; i++)
                        encByte[nBlock * 8 + i] = (byte) (orgBufByte[nBlock * 8 + i] ^ keyBytes[i]);
                }
                /* 길이가 8 이하 일 때 */
                else if (nBlock == 0) {
                    for (int i = 0; i < reminder; i++)
                        encByte[i] = (byte) (orgBufByte[i] ^ keyBytes[i]);
                }
            }
            /* 길이가 8의 배수 일 경우 */
            else if (reminder == 0) {
                encByte = cipher.doFinal(orgBufByte, 0, nBlock * 8);
            }

        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        }

        return encByte;
    }

    public byte[] Decryption(byte[] encByte) {
        byte[] decByte = null;
        try {
            Cipher cipher = Cipher.getInstance("DESede/ECB/NoPadding");
            cipher.init(Cipher.DECRYPT_MODE, secretKey);

            byte[] decBlockBuf = new byte[1024];
            decByte = new byte[encByte.length];

            int nBlock = encByte.length / 8;
            int reminder = encByte.length % 8;

            /* 데이터가 없을 때 */
            if (nBlock == 0 && reminder == 0)
                return decByte;

            /* 길이가 8의 배수가 아닐 때 */
            if (reminder != 0) {
                /* 길이가 8 이상 일 때 */
                if (nBlock != 0) {
                    decBlockBuf = cipher.doFinal(encByte, 0, nBlock * 8);

                    for (int i = 0; i < nBlock * 8; i++)
                        decByte[i] = decBlockBuf[i];

                    for (int i = 0; i < reminder; i++)
                        decByte[nBlock * 8 + i] = (byte) (encByte[nBlock * 8 + i] ^ keyBytes[i]);
                }
                /* 길이가 8 이하 일 때 */
                else if (nBlock == 0) {
                    for (int i = 0; i < reminder; i++)
                        decByte[nBlock * 8 + i] = (byte) (encByte[nBlock * 8 + i] ^ keyBytes[i]);
                }
            }
            /* 길이가 8의 배수 일 경우 */
            else if (reminder == 0) {
                decByte = cipher.doFinal(encByte, 0, nBlock * 8);
                ;
            }
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        }
        return decByte;
    }
}
