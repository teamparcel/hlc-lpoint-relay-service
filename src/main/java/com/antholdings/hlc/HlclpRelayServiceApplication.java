package com.antholdings.hlc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.WebApplicationInitializer;

@Configuration
@SpringBootApplication
@ComponentScan
@EnableAutoConfiguration
// for Weblogic
public class HlclpRelayServiceApplication extends SpringBootServletInitializer implements WebApplicationInitializer {
//public class HlclpRelayServiceApplication extends SpringBootServletInitializer {
//public class HlclpRelayServiceApplication {
    public static void main(String[] args) {
        SpringApplication.run(HlclpRelayServiceApplication.class, args);
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(applicationClass);
    }

    private static Class<HlclpRelayServiceApplication> applicationClass = HlclpRelayServiceApplication.class;
}