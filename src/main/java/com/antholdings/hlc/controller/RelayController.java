package com.antholdings.hlc.controller;

import com.antholdings.hlc.Item;
import com.antholdings.hlc.Packet;
import com.antholdings.hlc.util.TripleDes;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

/**
 * Created by hmludens on 2016. 3. 9..
 */
@Controller
@EnableAutoConfiguration
public class RelayController {
    private static int transUniqueNumber = 0;
    private static Date transDate = null;
    private String localhost;
    private String host;
    private boolean isDebug = false;

    @Value("${hlc.cluster1.ip}")
    String cluster1_ip;
    @Value("${hlc.cluster2.ip}")
    String cluster2_ip;

    @Value("${lpoint.server.host.dev}")
    String dev_host;
    @Value("${lpoint.server.host.live}")
    String live_host;

    @Value("${lpoint.server.cluster1.port.master}")
    String cluster1_master_port;
    @Value("${lpoint.server.cluster1.port.sub}")
    String cluster1_sub_port;
    @Value("${lpoint.server.cluster2.port.master}")
    String cluster2_master_port;
    @Value("${lpoint.server.cluster2.port.sub}")
    String cluster2_sub_port;

    @Value("${lpoint.protocol.headertype}")
    String protocolHeaderType;
    @Value("${lpoint.protocol.type.online}")
    String protocolType;
    @Value("${lpoint.protocol.companycode}")
    String protocolCompanyCode;
    @Value("${lpoint.protocol.membership.no}")
    String protocolMembershipNo;

    private byte[] intTo2ByteArray(int n) {
        byte[] data = new byte[2];
        data[0] = (byte) ((n >> 8) & 0xFF);
        data[1] = (byte) (n & 0xFF);
        return data;
    }

    private byte[] intToByteArray(int n) {
        byte[] data = new byte[4];
        data[0] = (byte) ((n & 0xFF000000) >> 24);
        data[1] = (byte) ((n & 0x00FF0000) >> 16);
        data[2] = (byte) ((n & 0x0000FF00) >> 8);
        data[3] = (byte) (n & 0x000000FF);
        return data;
    }

    public int byteToInt(byte[] arr) {
        if (arr.length == 4) {
            return (arr[0] & 0xff) << 24 | (arr[1] & 0xff) << 16 |
                    (arr[2] & 0xff) << 8 | (arr[3] & 0xff);
        } else if (arr.length == 2) {
            return (arr[0] & 0xff) << 8 | (arr[1] & 0xff);
        }
        return 0;
    }

    public String getEmptyString(int nLength) {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < nLength; i++) {
            sb.append(' ');
        }
        return sb.toString();
    }

    // Service ID :     KRNGPMCAO027
    // Interface ID :   IF-PM-A-004
    // Request No :     7000
    // Response No :    7001
    // Name :           회원 인증
    // Source System :  참여사
    // Target System :  멤버스
    @RequestMapping(value = "/", method = RequestMethod.POST)
    public
    @ResponseBody
    String authMemberByCardNumber(@RequestParam String cardnumber, HttpServletRequest request, HttpServletResponse response) {
        int nDataSize = 236;
        String returnString = "";
        String requestNo = "7000";
        Socket socket = null;
        OutputStream os = null;
        InputStream is = null;
        StringBuffer log = new StringBuffer();

        SimpleDateFormat dateformat = new SimpleDateFormat("yyyyMMdd");
        SimpleDateFormat timeformat = new SimpleDateFormat("HHmmss");

        if (isDebug) {
            host = dev_host;
        } else {
            host = live_host;
        }

        if(cardnumber.trim().isEmpty() || cardnumber.trim().length() != 16) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            returnString = "카드번호를 확인해주세요.";
        } else {
            // 전문 종료 문자 표식
            byte[] protocolEnd = new byte[]{
                    (byte) 0xFF, 0x0D, 0x0A
            };

            try {
                // 추적번호 생성을 위한 일자별 Unique Number 처리
                if (transDate == null) transDate = new Date();
                Date now = new Date();
                long diff = now.getTime() - transDate.getTime();
                int numOfDays = (int) (diff / (1000 * 60 * 60 * 24));
                if (numOfDays > 0) {
                    transDate = now;
                    transUniqueNumber = 0;
                }
                ++transUniqueNumber;

                Packet packet = new Packet();
                // 컨트롤 영역 : 헤더타입 (1자리)
                packet.addItem(new Item("헤더타입", 1, protocolHeaderType));
                // 컨트롤 영역 : 전문번호 (4자리)
                packet.addItem(new Item("전문번호", 4, requestNo));
                // 컨트롤 영역 : 기관코드 (4자리)
                packet.addItem(new Item("기관코드", 4, protocolCompanyCode));
                // 컨트롤 영역 : 전송일자 (8자리 YYYYMMDD)
                packet.addItem(new Item("전송일자", 8, dateformat.format(now)));
                // 컨트롤 영역 : 전송시간 (6자리 hhmmss)
                packet.addItem(new Item("전송시간", 6, timeformat.format(now)));
                // 컨트롤 영역 : 추적번호 (22자리)
                packet.addItem(new Item("추적번호", 22, String.format("%s%s%s%06d", requestNo, protocolCompanyCode, dateformat.format(now), transUniqueNumber)));
                // 컨트롤 영역 : 전문구분 (2자리)
                packet.addItem(new Item("전문구분", 2, protocolType));
                // 컨트롤 영역 : 응답코드 (공백 2자리)
                packet.addItem(new Item("응답코드", 2, getEmptyString(2)));
                // 컨트롤 영역 : 데이터 크기 (4자리)
                packet.addItem(new Item("데이터 크기", 4, String.format("%04d", nDataSize)));
                // 컨트롤 영역 : 시스템사용 (공백 20자리)
                packet.addItem(new Item("시스템사용", 20, getEmptyString(20)));
                // 데이터 영역 : 카드번호 (16자리)
                packet.addItem(new Item("카드번호", 16, cardnumber));
                // 데이터 영역 : 고객번호 (공백 10자리)
                packet.addItem(new Item("고객번호", 10, getEmptyString(10)));
                // 데이터 영역 : 제휴가맹점번호 (10자리)
                packet.addItem(new Item("제휴가맹점번호", 10, protocolMembershipNo));
                // 데이터 영역 : FILLER (공백 200자리)
                packet.addItem(new Item("FILLER", 200, getEmptyString(200)));

                log.append("Packet [" + packet.raw() + "]\n");

                // 전문 전체 길이 (컨트롤 영역 + 데이터 영역)
                byte[] byteTotalLength = new byte[2];
                byteTotalLength = intTo2ByteArray(packet.raw().length());

                log.append("Packet Length [" + byteToInt(byteTotalLength) + "] => " + Arrays.toString(byteTotalLength) + "\n");

                // 암호화는 전문을 암호화
                TripleDes tripleDes = new TripleDes();
                byte[] encodedData = tripleDes.Encryption(packet.raw());

                log.append("Encoded Packet [" + Arrays.toString(encodedData) + "]\n");

                localhost = InetAddress.getLocalHost().getHostAddress();

                log.append("Local IP [" + localhost + "]\n");
                if (localhost.equals(cluster1_ip)) {
                    try {
                        log.append("Connect to [" + host + ":" + cluster1_master_port + "]\n");
                        socket = new Socket(host, Integer.parseInt(cluster1_master_port));
                        if (socket.isConnected() == false) {
                            throw new Exception(host + ":" + cluster1_master_port + " connection failed.");
                        }
                    } catch (Exception ex) {
                        try {
                            log.append("Connect failed [" + ex.getMessage() + "]\n");
                            log.append("Connect to [" + host + ":" + cluster1_sub_port + "]\n");
                            socket = new Socket(host, Integer.parseInt(cluster1_sub_port));
                            if (socket.isConnected() == false) {
                                throw new Exception(host + ":" + cluster1_sub_port + " connection failed.");
                            }
                        } catch (Exception ex2) {
                            log.append("Connect failed [" + ex2.getMessage() + "]\n");
                        }
                    }
                } else if (localhost.equals(cluster2_ip)) {
                    try {
                        log.append("Connect to [" + host + ":" + cluster2_master_port + "]\n");
                        socket = new Socket(host, Integer.parseInt(cluster2_master_port));
                        if (socket.isConnected() == false) {
                            throw new Exception(host + ":" + cluster2_master_port + " connection failed.");
                        }
                    } catch (Exception ex) {
                        try {
                            log.append("Connect failed [" + ex.getMessage() + "]\n");
                            log.append("Connect to [" + host + ":" + cluster2_sub_port + "]\n");
                            socket = new Socket(host, Integer.parseInt(cluster2_sub_port));
                            if (socket.isConnected() == false) {
                                throw new Exception(host + ":" + cluster2_sub_port + " connection failed.");
                            }
                        } catch (Exception ex2) {
                            log.append("Connect failed [" + ex2.getMessage() + "]\n");
                        }
                    }
                }
                if (socket != null && socket.isConnected()) {
                    os = socket.getOutputStream();
                    os.write(byteTotalLength);
                    os.write(encodedData);
                    os.write(protocolEnd);
                    os.flush();

                    is = socket.getInputStream();

                    int nTotalLength = 0;

                    // read TotalLength
                    is.read(byteTotalLength);
                    nTotalLength = byteToInt(byteTotalLength);
                    log.append("Response Total Length [" + nTotalLength + "]\n");

                    byte[] responsePacket = new byte[nTotalLength];
                    int offset = 0;
                    while (offset < nTotalLength) {
                        int bytesRead = is.read(responsePacket, offset, nTotalLength - offset);
                        if (bytesRead == -1) {
                            // Incomplete packet
                        }
                        offset += bytesRead;
                    }

                    byte[] ended = new byte[3];
                    if (is.read(ended) == -1) {
                        log.append("Response Read to End");
                    }
                    log.append("Response End [" + Arrays.toString(ended) + "] [" + Arrays.toString(protocolEnd) + "]\n");

                    byte[] decodedData = tripleDes.Decryption(responsePacket);
                    byte[] btHeaderType = Arrays.copyOfRange(decodedData, 0, 1);
                    byte[] btRequestNo = Arrays.copyOfRange(decodedData, 1, 5);
                    byte[] btCompanyCode = Arrays.copyOfRange(decodedData, 5, 9);
                    byte[] btTransferDate = Arrays.copyOfRange(decodedData, 9, 17);
                    byte[] btTransferTime = Arrays.copyOfRange(decodedData, 17, 23);
                    byte[] btTraceNo = Arrays.copyOfRange(decodedData, 23, 45);
                    byte[] btProtocolType = Arrays.copyOfRange(decodedData, 45, 47);
                    byte[] btResponseCode = Arrays.copyOfRange(decodedData, 47, 49);
                    byte[] btDataSize = Arrays.copyOfRange(decodedData, 49, 53);
                    byte[] btSystemUsage = Arrays.copyOfRange(decodedData, 53, 73);
                    byte[] btCardNo = Arrays.copyOfRange(decodedData, 73, 89);
                    byte[] btCustomerNo = Arrays.copyOfRange(decodedData, 89, 99);
                    byte[] btMembershipNo = Arrays.copyOfRange(decodedData, 99, 109);
                    byte[] btAvailiablePoint = Arrays.copyOfRange(decodedData, 109, 118);
                    byte[] btRemainPointCode = Arrays.copyOfRange(decodedData, 118, 119);
                    byte[] btRemainPoint = Arrays.copyOfRange(decodedData, 119, 128);
                    byte[] btMessage = Arrays.copyOfRange(decodedData, 128, 192);
                    byte[] btMembershipCardCode = Arrays.copyOfRange(decodedData, 192, 202);
                    byte[] btMembersProductCode = Arrays.copyOfRange(decodedData, 202, 212);
                    byte[] btFiller = Arrays.copyOfRange(decodedData, 212, 487);

                    String responseCode = new String(btResponseCode);
                    responseCode = responseCode.trim();
                    String customerNo = new String(btCustomerNo);
                    customerNo = customerNo.trim();
                    String message = new String(btMessage);
                    message = message.trim();

                    log.append("Response [" + Arrays.toString(decodedData) + "]\n");
                    log.append("-------------------- Control -------------------\n");
                    log.append("HeaderType [" + new String(btHeaderType) + "]\n");
                    log.append("RequestNo [" + new String(btRequestNo) + "]\n");
                    log.append("CompanyCode [" + new String(btCompanyCode) + "]\n");
                    log.append("TransferDate [" + new String(btTransferDate) + "]\n");
                    log.append("TransferTime [" + new String(btTransferTime) + "]\n");
                    log.append("TraceNo [" + new String(btTraceNo) + "]\n");
                    log.append("ProtocolType [" + new String(btProtocolType) + "]\n");
                    log.append("ResponseCode [" + new String(btResponseCode) + "]\n");
                    log.append("DataSize [" + new String(btDataSize) + "]\n");
                    log.append("SystemUsage [" + new String(btSystemUsage) + "]\n");
                    log.append("-------------------- Data -------------------\n");
                    log.append("CardNo [" + new String(btCardNo) + "]\n");
                    log.append("CustomerNo [" + new String(btCustomerNo) + "]\n");
                    log.append("MembershipNo [" + new String(btMembershipNo) + "]\n");
                    log.append("AvailiablePoint [" + new String(btAvailiablePoint) + "]\n");
                    log.append("RemainPointCode [" + new String(btRemainPointCode) + "]\n");
                    log.append("RemainPoint [" + new String(btRemainPoint) + "]\n");
                    log.append("Message [" + new String(btMessage) + "]\n");
                    log.append("MembershipCardCode [" + new String(btMembershipCardCode) + "]\n");
                    log.append("MembersProductCode [" + new String(btMembersProductCode) + "]\n");
                    log.append("Filler [" + new String(btFiller) + "]\n");

                    if ("00".equalsIgnoreCase(responseCode)) {
                        returnString = customerNo;
                        response.setStatus(HttpServletResponse.SC_OK);
                    } else {
                        returnString = message.substring(4);
                        if ("22".equalsIgnoreCase(responseCode)) {
                            // 시스템 장애
                            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                        } else if ("44".equalsIgnoreCase(responseCode)) {
                            // 승인 거절
                            response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                        } else if ("77".equalsIgnoreCase(responseCode)) {
                            // 전문 오류
                            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                        } else if ("88".equalsIgnoreCase(responseCode)) {
                            // DB 미등록
                            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
                        } else if ("80".equalsIgnoreCase(responseCode)) {
                            // 운영사 DBMS 장애
                            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                        } else if ("92".equalsIgnoreCase(responseCode)) {
                            // 거래 중지
                            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                        } else if ("99".equalsIgnoreCase(responseCode)) {
                            // 거래 종료
                            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                        }
                    }
                } else {
                    log.append("Connect failed");
                    response.setStatus(HttpServletResponse.SC_GATEWAY_TIMEOUT);
                }
            } catch (Exception e) { // IOException
                e.printStackTrace(System.out);
                log.append("Error [" + e.getMessage() + "]");
                System.out.println(log.toString());
            } finally {
                if (os != null) try {
                    os.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if (is != null) try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if (socket != null) try {
                    socket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        returnString += "\n\nTrace -----\n" + log.toString();
        return returnString;
    }
}
