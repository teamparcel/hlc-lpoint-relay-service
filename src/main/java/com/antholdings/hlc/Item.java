package com.antholdings.hlc;

/**
 * Created by hmludens on 2016. 3. 10..
 */
public class Item {
    private String name;
    private int length;
    private String value;

    public Item(String name, int length, String value) {
        this.name = name;
        this.length = length;
        this.value = value;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public int getLength() {
        return length;
    }
    public void setLength(int length) {
        this.length = length;
    }
    public String getValue() {
        return value;
    }
    public void setValue(String value) {
        this.value = value;
    }

    public String raw() {
        StringBuffer padded = new StringBuffer(this.value);
        while (padded.toString().getBytes().length < this.length) {
            padded.append(' ');
        }
        return padded.toString();
    }
}
