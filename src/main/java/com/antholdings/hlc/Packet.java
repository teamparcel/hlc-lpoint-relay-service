package com.antholdings.hlc;

import java.util.ArrayList;

/**
 * Created by hmludens on 2016. 3. 10..
 */
public class Packet {
    private ArrayList<Item> items = new ArrayList<Item>();

    public void addItem(Item item) {
        this.items.add(item);
    }

    public Item getItem(int index) {
        return this.items.get(index);
    }

    public String raw() {
        StringBuffer result = new StringBuffer();
        for(Item item: items) {
            result.append(item.raw());
        }
        return result.toString();
    }
}
